# Structs and Interfaces

#### 1. What’s the difference between a method and a function?

 
#### 2. Why would you use an embedded anonymous field instead of a normal named field?
```
Para usar un método directamente como tipo contenedor.
```

#### 3. Add a new perimeter method to the Shape interface to calculate the perimeter of a shape. Implement the method for Circle and Rectangle.
```go
func (c *Circle) perimeter() float64 {
    return 2 * math.Pi * c.r
}

func (r *Rectangle) perimeter() float64 {
    l := distance(r.x1, r.y1, r.x1, r.y2)
    w := distance(r.x1, r.y1, r.x2, r.y1)
    return 2 * (l + w)
}
```

[ :arrow_left: Capítulo Anterior](/Capitulos/Chapter-6-Functions.md) - [Capítulo Siguiente :arrow_right: ](/Capitulos/Chapter-8-Packages.md)