# Concurrency
Los programas grandes a menudo se componen de muchos subprogramas más pequeños. Por ejemplo, un servidor web maneja las solicitudes realizadas desde navegadores web y sirve páginas web HTML en respuesta. Cada solicitud se maneja como un pequeño programa.

Sería ideal que los programas como estos puedan ejecutar sus componentes más pequeños al mismo tiempo (en el caso del servidor web, para manejar múltiples solicitudes). Avanzar en más de una tarea simultáneamente se conoce como concurrencia. Go tiene un amplio soporte para la concurrencia usando goroutines y canales.

#### 1. How do you specify the direction of a channel type?
Para específicar la dirección de un canal podemos hacerlo de las siguientes maneras:
```go
//receive-only
<-
//tambien:
<-chan int
//send-only
chan<- int
```
#### 2. Write your own Sleep function using time.After.
```go
func Sleep(duration time.Duration) {
    <-time.After(duration)
}
```


#### 3. What is a buffered channel? How would you create one with a capacity of 20?
Un buffered channel envía o recibe un mensaje sin esperar, es decir, es asincrono; solo si el canal esta lleno el envío esperará hasta que haya algún lugar para por lo menos un int.
Y para crear un buffered channel con una capacidad de 20 se hace de la siguiente manera:
```go
c := make(chan int, 20)
```

[ :arrow_left: Capítulo Anterior](/Capitulos/Chapter-9-Testing.md)