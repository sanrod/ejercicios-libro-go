# Introducing Go - Ejercicios

En este repositorio se encuentran los ejercicios resueltos del libro **Introducing Go** de *Caleb Doxsey* 

## Contenido

1. [Gettin Staarted](/Capitulos/Chapter-1-Getting-Started.md)
2. [Types](/Capitulos/Chapter-2-Types.md)
3. [Variables](Capitulos/Chapter-3-Variables.md)
4. [Control Structures](/Capitulos/Chapter-4-Control-Structures.md)
5. [Arrays, Slices, and Maps](/Capitulos/Chapter-5-Arrays-Slices-and-Maps.md)
6. [Functions](/Capitulos/Chapter-6-Functions.md)
7. [Structs and Interfaces](Capitulos/Chapter-7-Structs-and-Interfaces.md)
8. [Packages](/Capitulos/Chapter-8-Packages.md)
9. [Testing](/Capitulos/Chapter-9-Testing.md)
10. [Concurrency](/Capitulos/Chapter-10-Concurrency.md)


## Licencia 

Ver [Licencia](LICENSE)
